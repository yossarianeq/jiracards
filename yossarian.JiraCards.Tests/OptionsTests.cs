﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using yossarian.JiraCards.Contract;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Test
{
    class MockPasswordReader : IUiPasswordReader
    {
        public string ReadPassword(string label)
        {
            return "";
        }
    }

    [TestFixture]
    public class OptionsTests
    {
        [TestCase("-t OPS", null)]
        [TestCase("sometext", null)]
        [TestCase("-t OPS sometext", null)]
        [TestCase("", typeof(OptionsParsingException))]
        public void RequiresProjectOrAdditionalParametersTest(string cmdLine, Type exception)
        {
            Locator.Override<IUiPasswordReader, MockPasswordReader>();

            var provider = Locator.Get<IOptionsProvider>();
            var options = provider.Parse(cmdLine.Split(' '));
            var validator = Locator.Get<IOptionsValidator>();

            Assert.That(() => validator.Validate(options), 
                exception != null ? 
                    (IResolveConstraint)Throws.Exception.TypeOf(exception) : 
                    Throws.Nothing);
        }
    }
}
