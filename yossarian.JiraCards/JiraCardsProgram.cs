﻿using System.Collections.Generic;
using System.ServiceModel;
using Atlassian.Jira;
using yossarian.JiraCards.Contract.Interfaces;

namespace yossarian.JiraCards
{
    class JiraCardsProgram
    {
        private readonly IOptionsProvider _optionsProvider;
        private readonly IOptionsValidator _optionsValidator;
        private readonly IIssuesProviderFactory _issuesProviderFactory;
        private readonly IIssuesProcessorFactory _issuesProcessorFactory;

        public JiraCardsProgram(
            IOptionsProvider optionsProvider,
            IOptionsValidator optionsValidator,
            IIssuesProviderFactory issuesProviderFactory,
            IIssuesProcessorFactory issuesProcessorFactory)
        {
            _optionsProvider = optionsProvider;
            _optionsValidator = optionsValidator;
            _issuesProviderFactory = issuesProviderFactory;
            _issuesProcessorFactory = issuesProcessorFactory;
        }


        public void Execute(string[] args)
        {
            IEnumerable<Issue> issues;

            var options = _optionsProvider.Parse(args);
            _optionsValidator.Validate(options);

            var issuesProvider = _issuesProviderFactory.CreateIssuesProvider(options);

            try
            {
                issues = issuesProvider.GetIssues();
            }
            catch (EndpointNotFoundException ex)
            {
                throw new KnownMisconfigurationException(
                    $"Unable to connect to Jira server {options.JiraServer.ServerUrl}",
                    ex);
            }
            catch (FaultException ex) when (ex.Message.Contains("RemoteAuthenticationException"))
            {
                throw new KnownMisconfigurationException($"Invalid password for user {options.JiraServer.User}", ex);
            }


            var issuesProcessor = _issuesProcessorFactory.CreateIssuesProcessor(options);
            issuesProcessor.Process(issues);
        }
    }
}