﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Atlassian.Jira;
using Atlassian.Jira.Remote;
using Newtonsoft.Json;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards
{
    class SerializedIssues : IIssuesProcessor, IIssuesProvider
    {
        private readonly ExportImport _exportImport;

        public SerializedIssues(ExportImport exportImport)
        {
            if (exportImport == null) throw new ArgumentNullException(nameof(exportImport));
            _exportImport = exportImport;
        }

        public void Process(IEnumerable<Issue> issues)
        {
            if (issues == null) throw new ArgumentNullException(nameof(issues));
            using (var f = new StreamWriter(File.Open(_exportImport.ExportFileName, FileMode.Create, FileAccess.Write, FileShare.None)))
            {
                ExportIssues(issues, f);
            }
        }

        private void ExportIssues(IEnumerable<Issue> issues, TextWriter textWriter)
        {
            var issuesArray = issues.Select(x => x.ToRemote()).ToArray();
            var serializer = new JsonSerializer
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                Formatting = Formatting.Indented
            };

            serializer.Serialize(textWriter, issuesArray);
        }

        public IEnumerable<Issue> GetIssues()
        {
            using (var f = new StreamReader(File.Open(_exportImport.ImportFileName, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                return ImportIssues(f);
            }
        }

        private static IEnumerable<Issue> ImportIssues(TextReader file)
        {
            var serializer = new JsonSerializer();

            var remoteIssues = serializer.Deserialize<RemoteIssue[]>(new JsonTextReader(file));
            return remoteIssues.Select(issue => issue.ToLocal());
        }

    }
}