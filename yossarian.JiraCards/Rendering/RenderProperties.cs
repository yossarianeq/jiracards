﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Rendering
{
    sealed class RenderProperties
    {
        public RenderProperties(XCmSize issueSize, XCm minimalMargin, XCm fontSizeTitle, XCm fontSizeDescription, string fontFaceName, XCm textPadding)
        {
            BlackPen = new XPen(XColor.FromKnownColor(XKnownColor.Black), 1);
            BlackBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.Black));

            A4Width = new XCm(29.7);
            A4Height = new XCm(21.0);

            IssueSize = issueSize;
            MinimalMargin = minimalMargin;
            FontSizeTitle = fontSizeTitle;
            FontSizeDescription = fontSizeDescription;
            FontFaceName = fontFaceName;
            TextPadding = textPadding;

            FontRegular = new XFont(FontFaceName, FontSizeDescription.GetPoints(), XFontStyle.Regular, new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Default));
            FontBold = new XFont(FontFaceName, FontSizeTitle.GetPoints(), XFontStyle.Bold, new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Default));
            TextPaddingSize = new XCmSize(TextPadding, TextPadding);

            StringHeight = XCm.FromPoints(FontBold.GetHeight());
            StringHeightWithMargin = StringHeight + TextPadding;

            PageProperties = new PageProperties(new XCmSize(A4Width, A4Height), IssueSize, MinimalMargin);


        }

        public XFont FontBold { get; }
        public XFont FontRegular { get; }

        public XBrush BlackBrush { get; }
        public XPen BlackPen { get; }

        public XCmSize IssueSize { get; }
        public XCm MinimalMargin { get; }
        public XCm FontSizeTitle { get; }
        public XCm FontSizeDescription { get; }
        public string FontFaceName { get; }
        public XCm TextPadding { get; }

        public XCm A4Width { get; }
        public XCm A4Height { get; }
        public XCmSize TextPaddingSize { get; }

        public XCm StringHeight { get; }
        public XCm StringHeightWithMargin { get; }

        public PageProperties PageProperties { get; }
    }
}