﻿using System.Collections.Generic;
using Atlassian.Jira;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards
{
    class JiraIssuesProvider : IIssuesProvider
    {
        private readonly JiraServer _server;
        private readonly Filter _filter;

        public JiraIssuesProvider(JiraServer server, Filter filter)
        {
            _server = server;
            _filter = filter;
        }

        public IEnumerable<Issue> GetIssues()
        {
            var jira = new Jira(_server.ServerUrl, _server.User, _server.Password);
            var query = BuildQuery(_filter);
            return jira.GetIssuesFromJql(query);
        }

        private static string BuildQuery(Filter filter)
        {
            var queryParts = new List<string>();
            if (!string.IsNullOrWhiteSpace(filter.Project))
                queryParts.Add($"Project = \"{Escape(filter.Project)}\"");

            queryParts.Add("sprint in (openSprints())");
            queryParts.Add("Status != Closed");
            if (!string.IsNullOrWhiteSpace(filter.AdditionalQuery))
                queryParts.Add(filter.AdditionalQuery);

            var query = string.Join(" AND ", queryParts);

            return query + " ORDER BY id";
        }

        private static string Escape(string fieldValue) => fieldValue.Replace("\"", "\\\"");
    }
}