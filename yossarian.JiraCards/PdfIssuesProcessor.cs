﻿using System.Collections.Generic;
using System.IO;
using Atlassian.Jira;
using PdfSharp.Pdf;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;
using yossarian.JiraCards.Rendering;

namespace yossarian.JiraCards
{
    class PdfIssuesProcessor : IIssuesProcessor
    {
        private static PdfDocument CreateCards(IEnumerable<Issue> issues, PdfProperties pdfProperties)
        {
            

            var pageProperties = new RenderProperties
                (
                issueSize: new XCmSize(pdfProperties.IssueWidth, pdfProperties.IssueHeight),
                minimalMargin: pdfProperties.MinimalMargin,
                fontSizeTitle: pdfProperties.FontSize,
                fontSizeDescription: pdfProperties.FontSizeDescription,
                fontFaceName: pdfProperties.FontFaceName,
                textPadding: pdfProperties.TextPadding
                );

            var pageRenderer = new PageRenderer(pageProperties);

            return pageRenderer.CreateDocument(issues);
        }
        public void Process(IEnumerable<Issue> issues)
        {
            var doc = CreateCards(issues, new PdfProperties());
            var path = Path.GetTempFileName() + ".pdf";
            doc.Save(path);

            System.Diagnostics.Process.Start(path);
        }
    }
}