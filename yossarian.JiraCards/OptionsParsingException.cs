using System;
using System.Runtime.Serialization;

namespace yossarian.JiraCards
{
    [Serializable]
    public class OptionsParsingException : ApplicationException
    {
        public OptionsParsingException()
        {
        }

        public OptionsParsingException(string message) : base(message)
        {
        }

        protected OptionsParsingException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
    [Serializable]
    public class KnownMisconfigurationException : ApplicationException
    {
        public KnownMisconfigurationException()
        {
        }

        public KnownMisconfigurationException(string message) : base(message)
        {
        }

        public KnownMisconfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KnownMisconfigurationException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}