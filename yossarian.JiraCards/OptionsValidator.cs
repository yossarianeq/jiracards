﻿using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards
{
    class OptionsValidator : IOptionsValidator
    {
        public void Validate(Options options)
        {
            if (!string.IsNullOrWhiteSpace(options.ExportImport.ImportFileName))
                return;

            ValidateFilter(options.Filter);
        }

        private static void ValidateFilter(Filter filter)
        {
            if (string.IsNullOrWhiteSpace(filter.Project) && string.IsNullOrWhiteSpace(filter.AdditionalQuery))
                throw new OptionsParsingException("You need to specify either -t (project) parameter value, or additional query");
        }
    }
}
