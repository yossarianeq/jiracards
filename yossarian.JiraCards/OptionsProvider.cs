﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using CommandLine;
using CommandLine.Text;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;
using yossarian.JiraCards.Properties;

namespace yossarian.JiraCards
{
    class OptionsProvider : IOptionsProvider
    {
        private readonly IUiPasswordReader _uiPasswordReader;

        public OptionsProvider(IUiPasswordReader uiPasswordReader)
        {
            _uiPasswordReader = uiPasswordReader;
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
        class OptionsProxy
        {
            private IUiPasswordReader _uiPasswordReader;

            public OptionsProxy(IUiPasswordReader uiPasswordReader)
            {
                _uiPasswordReader = uiPasswordReader;
            }

            [Option('u', "user", Required = false, HelpText = "User used for connecting to Jira. If not specified, local one is used", DefaultValue = "")]
            public string User { get; set; }

            [Option('p', "password", Required = false, HelpText = "Password used for connecting to Jira. If not specified, prompt on CLI is done", DefaultValue = "")]
            public string Password { get; set; }

            [Option('s', "server", Required = false, HelpText = "Jira server", DefaultValue = "")]
            public string Jira { get; set; }

            [Option('t', "project", Required = false, HelpText = "Project for which you want the cards to be printed")]
            public string Project { get; set; }

            [Option('e', "export", DefaultValue = "", HelpText = "Exports the jira js to given file")]
            public string Export { get; set; }

            [Option('i', "import", DefaultValue = "", HelpText = "Imports tasks from given js file")]
            public string Import { get; set; }

            [Option('d', "debug", DefaultValue = false, HelpText = "Wait for debugger attached")]
            public bool WaitForDebugger { get; set; }

            [ValueOption(0)]
            public string AdditionalQuery { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                var help = new HelpText
                {
                    Heading = new HeadingInfo("JiraCards"),
                    Copyright = new CopyrightInfo("vit.tauer@solarwinds.com", 2015),
                    AdditionalNewLineAfterOption = true,
                    AddDashesToOption = true
                };

                help.AddOptions(this);

                if (LastParserState.Errors.Any())
                {
                    var errors = help.RenderParsingErrorsText(this, 2); // indent with two spaces

                    if (!string.IsNullOrEmpty(errors))
                    {
                        help.AddPreOptionsLine(string.Concat(Environment.NewLine, "ERROR(S):"));
                        help.AddPreOptionsLine(errors);
                    }
                }

                return help;
            }

            [ParserState]
            public IParserState LastParserState { get; set; }

            /// <summary>
            /// returns server, either from cmdline, or from settings
            /// </summary>
            /// <returns></returns>
            public string GetServer()
            {
                if (string.IsNullOrWhiteSpace(Jira))
                {
                    return Settings.Default.DefaultServer;
                }
                else
                {
                    return Jira;
                }
            }

            /// <summary>
            /// returns either user from cmdline, or name of current user
            /// </summary>
            /// <returns></returns>
            public string GetUser()
            {
                if (string.IsNullOrWhiteSpace(User))
                {
                    return RemoveDomain(WindowsIdentity.GetCurrent()?.Name);
                }
                else
                {
                    return User;
                }
            }

            /// <summary>
            /// Returns password from cmdline, if not there, asks
            /// </summary>
            /// <returns></returns>
            public string GetPassword(string user)
            {
                if (string.IsNullOrWhiteSpace(Password))
                {
                    Password = _uiPasswordReader.ReadPassword($"Password for user({user}): ");
                }

                return Password;
            }

            private static string RemoveDomain(string p)
            {
                if (p == null) throw new ArgumentNullException(nameof(p));

                var un = p.Split('\\');
                if (un.Length == 1)
                    return un[0];
                else
                    return un[1];
            }
        }

        public void WriteUsage(TextWriter output)
        {
            output.Write(new OptionsProxy(_uiPasswordReader).GetUsage());
        }
        
        public Options Parse(string[] args)
        {
            var opt = new OptionsProxy(_uiPasswordReader);
            if (Parser.Default.ParseArguments(args, opt))
            {
                if (opt.WaitForDebugger)
                {
                    Console.WriteLine("Waiting for debugger to be attached ...");
                    while (!Debugger.IsAttached)
                    {
                        Thread.Sleep(1);
                    }
                }

                var user = opt.GetUser();
                var pwd = opt.GetPassword(user);

                var js = new JiraServer(user, pwd, opt.GetServer());
                var filter = new Filter(opt.Project, opt.AdditionalQuery);
                var exportImport = new ExportImport(opt.Export, opt.Import);

                return new Options(js, filter, exportImport);
            }
            else
            {
                throw new OptionsParsingException();
            }
        }
    }
}
