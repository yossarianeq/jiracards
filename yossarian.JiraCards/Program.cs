﻿using System;
using System.ServiceModel;
using yossarian.JiraCards.Contract;

namespace yossarian.JiraCards
{
    static class Program
    {
        static int Main(string[] args)
        {
            try
            {
                var program = Locator.Get<JiraCardsProgram>();
                program.Execute(args);
            }
            catch (OptionsParsingException oex)
            {
                if (!string.IsNullOrWhiteSpace(oex.Message))
                    Console.Error.WriteLine(oex.Message);

                return 1;
            }
            catch (KnownMisconfigurationException kex)
            {
                Console.Error.WriteLine(kex.Message);
                return 1;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Fatal error:");
                Console.Error.WriteLine(ex);
                return 1;
            }

            return 0;
        }
    }
}
