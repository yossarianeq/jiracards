﻿using Ninject.Modules;
using yossarian.JiraCards.Contract.Interfaces;

namespace yossarian.JiraCards.Contract
{
    class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IOptionsValidator>().To<OptionsValidator>();
            Bind<IOptionsProvider>().To<OptionsProvider>();
            Bind<IIssuesProviderFactory>().To<IssuesProviderFactory>();
            Bind<IIssuesProcessorFactory>().To<IssuesProcessorFactory>();
            Bind<IUiPasswordReader>().To<ConsolePasswordReader>();
        }
    }
}
