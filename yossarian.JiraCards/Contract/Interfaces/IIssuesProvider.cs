﻿using System.Collections.Generic;
using Atlassian.Jira;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IIssuesProvider
    {
        IEnumerable<Issue> GetIssues();
    }
}
