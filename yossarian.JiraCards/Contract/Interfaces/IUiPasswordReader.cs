﻿namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IUiPasswordReader
    {
        string ReadPassword(string label);
    }
}