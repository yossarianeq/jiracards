﻿using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IIssuesProcessorFactory
    {
        IIssuesProcessor CreateIssuesProcessor(Options options);
    }
}