using System.IO;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IOptionsProvider
    {
        Options Parse(string[] args);
        void WriteUsage(TextWriter output);
    }
}