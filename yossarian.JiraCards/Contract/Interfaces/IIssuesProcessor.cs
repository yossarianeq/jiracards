﻿using System.Collections.Generic;
using Atlassian.Jira;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IIssuesProcessor
    {
        void Process(IEnumerable<Issue> issues);
    }
}