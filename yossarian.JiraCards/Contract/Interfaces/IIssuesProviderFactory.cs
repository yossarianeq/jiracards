﻿using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IIssuesProviderFactory
    {
        IIssuesProvider CreateIssuesProvider(Options options);
    }
}