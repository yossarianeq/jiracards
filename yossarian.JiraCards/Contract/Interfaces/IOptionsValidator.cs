﻿using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards.Contract.Interfaces
{
    interface IOptionsValidator
    {
        /// <summary>
        /// Throws on issue
        /// </summary>
        /// <param name="options"></param>
        void Validate(Options options);
    }
}
