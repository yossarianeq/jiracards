﻿namespace yossarian.JiraCards.Contract.Models
{
    sealed class PdfProperties
    {
        // rectangle size

        public XCm IssueHeight => new XCm(6.0); // cm
        public XCm IssueWidth => new XCm(13.0); // cm

        public XCm MinimalMargin => new XCm(1.0); // cm
        public XCm TextPadding => new XCm(0.2); // cm

        public XCm FontSize => new XCm(0.8); // cm, size of em
        public XCm FontSizeDescription => new XCm(0.7); // cm, size of em
        public string FontFaceName => "Arial";
    }
}