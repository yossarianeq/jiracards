namespace yossarian.JiraCards.Contract.Models
{
    public sealed class ExportImport
    {
        public ExportImport(string exportFileName, string importFileName)
        {
            ExportFileName = exportFileName;
            ImportFileName = importFileName;
        }

        public string ExportFileName { get; }

        public string ImportFileName { get; }

    }
}