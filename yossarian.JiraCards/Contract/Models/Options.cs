﻿using System;

namespace yossarian.JiraCards.Contract.Models
{
    public sealed class Options
    {
        public Options(JiraServer jiraServer, Filter filter, ExportImport exportImport)
        {
            if (jiraServer == null) throw new ArgumentNullException(nameof(jiraServer));
            if (filter == null) throw new ArgumentNullException(nameof(filter));
            if (exportImport == null) throw new ArgumentNullException(nameof(exportImport));

            JiraServer = jiraServer;
            Filter = filter;
            ExportImport = exportImport;
        }

        public JiraServer JiraServer { get; }

        public Filter Filter { get; }
        public ExportImport ExportImport { get;}
    }
}