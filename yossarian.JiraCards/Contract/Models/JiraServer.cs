namespace yossarian.JiraCards.Contract.Models
{
    public sealed class JiraServer
    {
        public JiraServer(string user, string password, string serverUrl)
        {
            User = user;
            Password = password;
            ServerUrl = serverUrl;
        }

        public string User { get; }
        public string Password { get; }
        public string ServerUrl { get; }
    }
}