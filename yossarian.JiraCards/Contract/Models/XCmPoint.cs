﻿using PdfSharp.Drawing;

namespace yossarian.JiraCards.Contract.Models
{
    // point, but in cms
    sealed class XCmPoint
    {
        public XCmPoint(XCm cmX, XCm cmY)
        {
            X = cmX;
            Y = cmY;
        }
        public XCm X { get; }
        public XCm Y { get; }

        public static XCmPoint operator +(XCmPoint p1, XCmSize p2)
        {
            return new XCmPoint(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static implicit operator XPoint(XCmPoint p)
        {
            return new XPoint(p.X.GetPoints(), p.Y.GetPoints());
        }
    }
}
