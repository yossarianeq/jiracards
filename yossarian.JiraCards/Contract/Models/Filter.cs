namespace yossarian.JiraCards.Contract.Models
{
    public sealed class Filter
    {
        public Filter(string project, string additionalQuery)
        {
            Project = project;
            AdditionalQuery = additionalQuery;
        }

        public string Project { get; }
        public string AdditionalQuery { get; }
    }
}