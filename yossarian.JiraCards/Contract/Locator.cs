﻿using Ninject;

namespace yossarian.JiraCards.Contract
{
    public static class Locator
    {
        private static readonly IKernel _kernel = new StandardKernel();
        static Locator()
        {
            _kernel.Load(new Bindings());
        }
        public static T Get<T>()
        {
            return _kernel.Get<T>();
        }

        public static void Override<TInterface, TBinding>() where TBinding : TInterface
        {
            _kernel.Rebind<TInterface>().To<TBinding>();
        }
    }
}
