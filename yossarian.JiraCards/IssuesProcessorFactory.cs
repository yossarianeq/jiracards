﻿using System;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards
{
    class IssuesProcessorFactory : IIssuesProcessorFactory
    {
        public IIssuesProcessor CreateIssuesProcessor(Options options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (!string.IsNullOrWhiteSpace(options.ExportImport.ExportFileName))
            {
                return new SerializedIssues(options.ExportImport);
            }
            else
            {
                return new PdfIssuesProcessor();
            }
        }
    }
}