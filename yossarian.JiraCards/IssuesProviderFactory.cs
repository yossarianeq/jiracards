﻿using System;
using yossarian.JiraCards.Contract.Interfaces;
using yossarian.JiraCards.Contract.Models;

namespace yossarian.JiraCards
{
    class IssuesProviderFactory : IIssuesProviderFactory
    {
        public IIssuesProvider CreateIssuesProvider(Options options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (!string.IsNullOrWhiteSpace(options.ExportImport.ImportFileName))
            {
                return new SerializedIssues(options.ExportImport);
            }
            else
            {
                return new JiraIssuesProvider(options.JiraServer, options.Filter);
            }
        }
    }
}